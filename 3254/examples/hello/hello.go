// experiment to call functions from other files

package main

import "fmt"

func haveFun() {
	fmt.Println("having fun")
}

func main() {
	var var1 = "hello from var1"
	fmt.Println(var1)

	var2 := "hello from var2"
	fmt.Println(var2)

	var var3 string
	fmt.Println(var3)

	var var4 string = "hello from var4"
	fmt.Println(var4)

	foo()

	haveFun()
}
