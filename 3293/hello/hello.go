package main

import (
	"fmt"
	"time"
)

func main() {
	go hello()
	time.Sleep(2 * time.Second)
	fmt.Println("Main done!")
}

func hello() {
	fmt.Println("Hi!")
	time.Sleep(1 * time.Second)
}
