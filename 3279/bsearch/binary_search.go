// binary search program in go lang
package main

import "fmt"

func binarySearch(arr []int, low int, high int, num int) int {
	if low >= high {
		return -1
	}

	// calculating middle index
	mid := low + (high-low)/2

	if num == arr[mid] {
		return mid
	} else if num < arr[mid] {
		return binarySearch(arr, low, mid-1, num)
	} else {
		return binarySearch(arr, mid+1, high, num)
	}

}

func main() {
	length := 0
	fmt.Println("Enter the length of array:")
	fmt.Scanln(&length)
	fmt.Println("Enter the elements")
	arr := make([]int, length)
	for i := 0; i < length; i++ {
		fmt.Scanln(&arr[i])
	}

	var num int
	fmt.Print("Enter number to search: ")
	fmt.Scan(&num)

	index := binarySearch(arr, 0, 7, num)

	if index == -1 {
		fmt.Println("Number not found!")
	} else {
		fmt.Printf("Found at index %d", index)
	}
}
