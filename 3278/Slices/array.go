// slice program: operations on array and map
package main

import "fmt"

func main() {
	array1 := [5]int{4, 5, 6}
	slice1 := []int{1, 2}
	fmt.Println(array1)
	fmt.Println(slice1)
	slice1 = append(slice1, 6)
	fmt.Println(slice1)
	fmt.Println(slice1[1:2])
	fmt.Println(slice1[1:3])
	map1 := map[string]int{
		"foo": 2,
		"bar": 23,
	}
	fmt.Println(map1)
	fmt.Println(map1["foo"])
	map1["searce"] = 12
	fmt.Println(map1)

}
